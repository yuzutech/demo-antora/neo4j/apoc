= APOC User Guide {apoc-release-absolute}
:toc: left
:experimental:
:sectid:
:sectlinks:
:toclevels: 2
:img: https://raw.githubusercontent.com/neo4j-contrib/neo4j-apoc-procedures/{branch}/docs/images
//{imagesdir}
:script: https://raw.githubusercontent.com/neo4j-contrib/neo4j-apoc-procedures/{branch}/docs/script
:gh-docs: https://neo4j-contrib.github.io/neo4j-apoc-procedures

[abstract]
--
This is the user guide for Neo4j APOC {docs-version}, authored by the Neo4j Labs Team.
--

The guide covers the following areas:

* xref:introduction.adoc[] -- An Introduction to the APOC library.
* xref:help.adoc[] -- In-built help in the library.
* xref:overview.adoc[] -- A list of all APOC procedures and functions.
* xref:config.adoc[] -- Configuration options used by the library.
//* <<user-defined-functions>> -- An overview of user defined functions available in the library.
* xref:import.adoc[] -- A detailed guide to procedures that can be used to import data from different formats including JSON, CSV, and XLS.
* <<export>> -- A detailed guide to procedures that can be used to export data to different formats including JSON, CSV, GraphML, and Gephi.
* <<database-integration>> -- A detailed guide to procedures that can be used to integrate with other databases including relational databases, MongoDB, Couchbase, and ElasticSearch.
* <<graph-updates>> -- A detailed guide to procedures that can be used to apply graph updates.
* <<data-structures>> -- A detailed guide to procedures and functions, that can be used to work with data structures.
* <<temporal>>-- A detailed guide to procedures that can be used to format temporal types.
* <<mathematical>> -- A detailed guide to procedures and functions that can be used for mathematical operations.
* <<path-finding>> -- A detailed guide to procedures that can be used for advanced graph querying.
* <<comparing-graphs>> -- A detailed guide to procedures that can be used to compare graphs.
* <<cypher-execution>> -- A detailed guide to procedures that can be used for Cypher scripting.
* <<virtual>> -- A detailed guide to procedures that can be used to create virtual nodes and relationships.
* <<job-management>> -- A detailed guide to procedures that can be used for background job management.
* <<database-introspection>> -- A detailed guide to procedures that can be used to introspect the database.
* <<operational>> -- A detailed guide to operational procedures.
* <<misc>> -- A detailed guide to miscellaneous procedures and functions, including map and collection functions, text functions, and spatial functionality.
* <<indexes>> -- A detailed guide to indexing procedures.
* <<algorithms>> -- A detailed guide to Graph Algorithms.

include::_import.adoc[]
